﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using IXBI.Net.Helper;

namespace IXBI.Net.Access
{
    public class SqlAccess
    {
        #region Properties Connection

        public string ConnectionString { get; set; } = string.Empty;
        public int CommandTimeout { get; set; } = 0;

        #endregion Properties Connection

        #region Constructor Connection

        public SqlAccess()
        {
        }

        public SqlAccess(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public SqlAccess(string connectionString, int commandTimeout)
        {
            ConnectionString = connectionString;
            CommandTimeout = commandTimeout;
        }

        #endregion Constructor Connection

        #region Execute Select Data

        public List<T> GetDataList<T>(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        List<T> objectList = ModelHelper.CreateObjectList<T>(dt);

                        return objectList;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T GetDataObject<T>(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        T objectNew = ModelHelper.CreateObject<T>(dt);

                        return objectNew;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTable(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        return dt;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetDataSet(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        return ds;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Execute Select Data

        #region Execute Select Data with Paging

        public List<T> GetDataListPaging<T>(SqlCommand command, Paging paging)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds, paging.StartRecord, paging.PageSize, "Paging");

                        DataTable dt = new DataTable();
                        if (ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                        }

                        dt.Columns.Add("Page");
                        dt.Columns.Add("PageSize");
                        dt.Columns.Add("TotalRow");

                        if (dt.Rows.Count > 0)
                        {
                            DataTable dtTotalRow = new DataTable();
                            da.Fill(dtTotalRow);

                            dt.Rows[0]["Page"] = paging.PageNumber;
                            dt.Rows[0]["PageSize"] = paging.PageSize;
                            dt.Rows[0]["TotalRow"] = dtTotalRow.Rows.Count;
                        }

                        List<T> objectList = ModelHelper.CreateObjectList<T>(dt);

                        return objectList;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTablePaging(SqlCommand command, Paging paging)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds, paging.StartRecord, paging.PageSize, "Paging");

                        DataTable dt = new DataTable();
                        if (ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                        }

                        dt.Columns.Add("Page");
                        dt.Columns.Add("PageSize");
                        dt.Columns.Add("TotalRow");

                        if (dt.Rows.Count > 0)
                        {
                            DataTable dtTotalRow = new DataTable();
                            da.Fill(dtTotalRow);

                            dt.Rows[0]["Page"] = paging.PageNumber;
                            dt.Rows[0]["PageSize"] = paging.PageSize;
                            dt.Rows[0]["TotalRow"] = dtTotalRow.Rows.Count;
                        }

                        return dt;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> GetDataListPagingExecuteSqlString<T>(SqlCommand command, Paging paging)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    string sql = command.CommandText;
                    string sqlCount = string.Format("SELECT COUNT(*) FROM ( {0} ) TEMP", sql);

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlCount;

                    int totalRow = Convert.ToInt32(command.ExecuteScalar());

                    string sqlSortBy = string.Format("{0} {1}", paging.SortBy, paging.SortDirection);
                    string Paging = string.Format(@"SELECT *
                                            FROM (SELECT ROW_NUMBER() OVER ( ORDER BY {0} ) AS ROW_NUM, *
                                            FROM ( {1} ) TEMP) TEMP2
                                            WHERE ROW_NUM > @StartRecord AND ROW_NUM <= @EndRecord", sqlSortBy, sql);

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;
                    command.CommandType = CommandType.Text;
                    command.CommandText = Paging;

                    command.Parameters.Add(new SqlParameter("@StartRecord", paging.StartRecord));
                    command.Parameters.Add(new SqlParameter("@EndRecord", paging.EndRecord));

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        dt.Columns.Add("Page");
                        dt.Columns.Add("PageSize");
                        dt.Columns.Add("TotalRow");

                        if (dt.Rows.Count > 0)
                        {
                            dt.Rows[0]["Page"] = paging.PageNumber;
                            dt.Rows[0]["PageSize"] = paging.PageSize;
                            dt.Rows[0]["TotalRow"] = totalRow;
                        }

                        List<T> objectList = ModelHelper.CreateObjectList<T>(dt);

                        return objectList;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTablePagingExecuteSqlString(SqlCommand command, Paging paging)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    string sql = command.CommandText;
                    string sqlCount = string.Format("SELECT COUNT(*) FROM ( {0} ) TEMP", sql);

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlCount;

                    int totalRow = Convert.ToInt32(command.ExecuteScalar());

                    string sqlSortBy = string.Format("{0} {1}", paging.SortBy, paging.SortDirection);
                    string Paging = string.Format(@"SELECT *
                                            FROM (SELECT ROW_NUMBER() OVER ( ORDER BY {0} ) AS ROW_NUM, *
                                            FROM ( {1} ) TEMP) TEMP2
                                            WHERE ROW_NUM > @StartRecord AND ROW_NUM <= @EndRecord", sqlSortBy, sql);

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;
                    command.CommandType = CommandType.Text;
                    command.CommandText = Paging;

                    command.Parameters.Add(new SqlParameter("@StartRecord", paging.StartRecord));
                    command.Parameters.Add(new SqlParameter("@EndRecord", paging.EndRecord));

                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        dt.Columns.Add("Page");
                        dt.Columns.Add("PageSize");
                        dt.Columns.Add("TotalRow");

                        if (dt.Rows.Count > 0)
                        {
                            dt.Rows[0]["Page"] = paging.PageNumber;
                            dt.Rows[0]["PageSize"] = paging.PageSize;
                            dt.Rows[0]["TotalRow"] = totalRow;
                        }

                        return dt;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Execute Select Data with Paging

        #region Execute Select Scalar

        public T GetDataScalar<T>(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;

                    T objectScalar = (T)command.ExecuteScalar();

                    return objectScalar;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Execute Select Scalar

        #region Execute Insert, Update, Delete

        public int SaveChange(SqlCommand command)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    command.Connection = connection;
                    command.CommandTimeout = CommandTimeout;

                    int rowsEffect = command.ExecuteNonQuery();

                    return rowsEffect;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Execute Insert, Update, Delete

        #region Execute BulkCopy

        public void BulkCopyDataList<T>(List<T> data, string tableName)
        {
            try
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(ConnectionString, SqlBulkCopyOptions.Default))
                {
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.BulkCopyTimeout = CommandTimeout;

                    using (DataTable dataTable = new DataTable())
                    {
                        List<object> primaryKey = new List<object>();

                        foreach (var fieldInfo in typeof(T).GetProperties())
                        {
                            dataTable.Columns.Add(new DataColumn()
                            {
                                ColumnName = fieldInfo.Name,
                                DataType = fieldInfo.PropertyType
                            });

                            if (Attribute.GetCustomAttribute(fieldInfo, typeof(KeyAttribute)) != null)
                            {
                                primaryKey.Add(dataTable.Columns[fieldInfo.Name]);
                            }

                            bulkCopy.ColumnMappings.Add(fieldInfo.Name, fieldInfo.Name);
                        }

                        if (primaryKey.Any())
                        {
                            DataColumn[] _keyCollection = new DataColumn[primaryKey.Count];

                            for (int i_item = 0; i_item <= primaryKey.Count - 1; i_item++)
                            {
                                _keyCollection[i_item] = (DataColumn)primaryKey[i_item];
                            }

                            dataTable.PrimaryKey = _keyCollection;
                        }

                        foreach (var item in data)
                        {
                            DataRow row = dataTable.NewRow();
                            foreach (var fieldInfo in typeof(T).GetProperties())
                            {
                                row[fieldInfo.Name] = fieldInfo.GetValue(item, null);
                            }
                            dataTable.Rows.Add(row);
                        }

                        bulkCopy.WriteToServer(dataTable);

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BulkCopyDataTable(DataTable dt, string tableName)
        {
            try
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(ConnectionString, SqlBulkCopyOptions.Default))
                {
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.BulkCopyTimeout = CommandTimeout;
                    bulkCopy.WriteToServer(dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Execute Transaction Scope

        public TransactionScope Scope { get; set; } = null;

        public void OpenTransactionScope()
        {
            try
            {
                if (Scope == null)
                {
                    Scope = new TransactionScope();
                }
                else
                {
                    Scope.Dispose();
                    Scope = new TransactionScope();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void OpenTransactionScope(System.Transactions.IsolationLevel TransactionScopeLevel)
        {
            try
            {
                TransactionOptions transactionOptions = new TransactionOptions();
                transactionOptions.IsolationLevel = TransactionScopeLevel;

                if (Scope == null)
                {
                    Scope = new TransactionScope(TransactionScopeOption.RequiresNew, transactionOptions);
                }
                else
                {
                    Scope.Dispose();
                    Scope = new TransactionScope(TransactionScopeOption.RequiresNew, transactionOptions);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void OpenTransactionScope(int transactionTimeout)
        {
            try
            {
                TimeSpan timeout = TimeSpan.FromSeconds(transactionTimeout);

                SetTransactionManagerField("_cachedMaxTimeout", true);
                SetTransactionManagerField("_maximumTimeout", timeout);

                if (Scope == null)
                {
                    Scope = new TransactionScope(TransactionScopeOption.RequiresNew, timeout);
                }
                else
                {
                    Scope.Dispose();
                    Scope = new TransactionScope(TransactionScopeOption.RequiresNew, timeout);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void OpenTransactionScope(System.Transactions.IsolationLevel TransactionScopeLevel, int transactionTimeout)
        {
            try
            {
                TimeSpan timeout = TimeSpan.FromSeconds(transactionTimeout);

                SetTransactionManagerField("_cachedMaxTimeout", true);
                SetTransactionManagerField("_maximumTimeout", timeout);

                TransactionOptions transactionOptions = new TransactionOptions();
                transactionOptions.IsolationLevel = TransactionScopeLevel;
                transactionOptions.Timeout = timeout;

                if (Scope == null)
                {
                    Scope = new TransactionScope(TransactionScopeOption.RequiresNew, transactionOptions);
                }
                else
                {
                    Scope.Dispose();
                    Scope = new TransactionScope(TransactionScopeOption.RequiresNew, transactionOptions);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetTransactionManagerField(string fieldName, object value)
        {
            try
            {
                typeof(TransactionManager).GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CloseTransactionScope()
        {
            try
            {
                if (Scope != null)
                {
                    Scope.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CommitTransactionScope()
        {
            try
            {
                if (Scope != null)
                {
                    Scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Execute Transaction Scope
    }
}