﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IXBI.Net.Helper
{
    public class FtpHelper
    {
        public string FtpUrl { get; set; } = string.Empty;
        public string FtpUserName { get; set; } = string.Empty;
        public string FtpPassword { get; set; } = string.Empty;

        public string UploadFile(string fileName, string uploadDirectory)
        {
            try
            {
                string PureFileName = new FileInfo(fileName).Name;
                string uploadUrl = string.Format("{0}{1}/{2}", FtpUrl, uploadDirectory, PureFileName);
                FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(uploadUrl);
                req.Proxy = null;
                req.Method = WebRequestMethods.Ftp.UploadFile;
                req.Credentials = new NetworkCredential(FtpUserName, FtpPassword);
                req.UseBinary = true;
                req.UsePassive = true;
                byte[] data = File.ReadAllBytes(fileName);
                req.ContentLength = data.Length;
                Stream stream = req.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();
                FtpWebResponse res = (FtpWebResponse)req.GetResponse();
                return res.StatusDescription;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DownloadFile(string fileNameDownload, string tempDirPath)
        {
            try
            {
                string ResponseDescription = "";
                string PureFileName = new FileInfo(fileNameDownload).Name;
                string DownloadedFilePath = tempDirPath + "/" + PureFileName;
                string downloadUrl = String.Format("{0}/{1}", FtpUrl, fileNameDownload);
                FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(downloadUrl);
                req.Method = WebRequestMethods.Ftp.DownloadFile;
                req.Credentials = new NetworkCredential(FtpUserName, FtpPassword);
                req.UseBinary = true;
                req.Proxy = null;

                FtpWebResponse response = (FtpWebResponse)req.GetResponse();
                Stream stream = response.GetResponseStream();
                byte[] buffer = new byte[2048];
                FileStream fs = new FileStream(DownloadedFilePath, FileMode.Create);
                int ReadCount = stream.Read(buffer, 0, buffer.Length);

                while (ReadCount > 0)
                {
                    fs.Write(buffer, 0, ReadCount);
                    ReadCount = stream.Read(buffer, 0, buffer.Length);
                }

                ResponseDescription = response.StatusDescription;
                fs.Close();
                stream.Close();

                return ResponseDescription;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
