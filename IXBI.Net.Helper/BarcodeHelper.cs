﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using iTextSharp.text.pdf;
using System.Drawing;
using System.Drawing.Imaging;

namespace IXBI.Net.Helper
{
    public class BarcodeHelper
    {
        public static Image RenderBarcodeToImage(string text)
        {
            try
            {
                Barcode128 clsbarcode = new Barcode128();
                clsbarcode.Code = text;
                clsbarcode.X = 0.8F;
                clsbarcode.InkSpreading = 0.0F;
                clsbarcode.N = 3.0F;
                clsbarcode.Size = 10.0F;
                clsbarcode.Baseline = 10.0F;
                clsbarcode.BarHeight = 28.0F;
                clsbarcode.GenerateChecksum = false;
                clsbarcode.ChecksumText = false;
                clsbarcode.StartStopText = true;
                clsbarcode.Extended = false;

                Image image = clsbarcode.CreateDrawingImage(Color.Black, Color.White);

                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static byte[] RenderBarcodeToByte(string text)
        {
            try
            {
                Image image = RenderBarcodeToImage(text);
                byte[] barcodeByte = null;
                if (image != null)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        image.Save(mStream, ImageFormat.Bmp);
                        barcodeByte = mStream.ToArray();
                    }
                }

                return barcodeByte;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string RenderBarcodeToBase64String(string text)
        {
            try
            {
                byte[] imageBytes = RenderBarcodeToByte(text);
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
