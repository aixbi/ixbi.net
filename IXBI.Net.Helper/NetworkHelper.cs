﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;

namespace IXBI.Net.Helper
{
    public class NetworkHelper
    {
        private const int PING_TIMEOUT = 1000;

        [System.Runtime.InteropServices.DllImport("iphlpapi.dll", ExactSpelling = true)]
        private static extern int SendARP(int DestIP, int SrcIP, byte[] pMacAddr, ref int PhyAddrLen);

        private static PhysicalAddress GetMacAddress(IPAddress ipAddress)
        {
            try
            {
                const int MacAddressLength = 6;
                int length = MacAddressLength;
                var macBytes = new byte[MacAddressLength];
                SendARP(BitConverter.ToInt32(ipAddress.GetAddressBytes(), 0), 0, macBytes, ref length);
                return new PhysicalAddress(macBytes);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private static string GetIPAddress(bool GetLan = false)
        {
            try
            {
                string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (String.IsNullOrEmpty(visitorIPAddress))
                    visitorIPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                if (string.IsNullOrEmpty(visitorIPAddress))
                    visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

                if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
                {
                    GetLan = true;
                    visitorIPAddress = string.Empty;
                }

                if (GetLan)
                {
                    if (string.IsNullOrEmpty(visitorIPAddress))
                    {
                        //This is for Local(LAN) Connected ID Address
                        string stringHostName = Dns.GetHostName();
                        //Get Ip Host Entry
                        IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                        //Get Ip Address From The Ip Host Entry Address List
                        IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                        try
                        {
                            visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                        }
                        catch
                        {
                            try
                            {
                                visitorIPAddress = arrIpAddress[0].ToString();
                            }
                            catch
                            {
                                try
                                {
                                    arrIpAddress = Dns.GetHostAddresses(stringHostName);
                                    visitorIPAddress = arrIpAddress[0].ToString();
                                }
                                catch
                                {
                                    visitorIPAddress = "127.0.0.1";
                                }
                            }
                        }
                    }
                }
                return visitorIPAddress;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetMacAddress(string ipAddress)
        {
            try
            {
                string macAddress = string.Empty;
                if (!IsHostAccessible(ipAddress)) return null;
                try
                {
                    ProcessStartInfo processStartInfo = new ProcessStartInfo();
                    Process process = new Process();
                    processStartInfo.FileName = "nbtstat";
                    processStartInfo.RedirectStandardInput = false;
                    processStartInfo.RedirectStandardOutput = true;
                    processStartInfo.Arguments = "-a " + ipAddress;
                    processStartInfo.UseShellExecute = false;
                    process = Process.Start(processStartInfo);
                    int Counter = -1;
                    while (Counter <= -1)
                    {
                        Counter = macAddress.Trim().ToLower().IndexOf("mac address", 0);
                        if (Counter > -1)
                        {
                            break;
                        }
                        macAddress = process.StandardOutput.ReadLine();
                    }
                    process.WaitForExit();
                    macAddress = macAddress.Trim();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed because:" + e.ToString());
                }
                return macAddress;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool IsHostAccessible(string hostNameOrAddress)
        {
            try
            {
                Ping ping = new Ping();
                PingReply reply = ping.Send(hostNameOrAddress, PING_TIMEOUT);
                return reply.Status == IPStatus.Success;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        private static extern int SendARP(int DestIP, int SrcIP, byte[] pMacAddr, ref uint PhyAddrLen);

        private string GetMACAddressFromARP(string hostNameOrAddress)
        {
            try
            {
                if (!IsHostAccessible(hostNameOrAddress)) return null;
                IPHostEntry hostEntry = Dns.GetHostEntry(hostNameOrAddress);
                if (hostEntry.AddressList.Length == 0)
                    return null;
                byte[] macAddr = new byte[6];
                uint macAddrLen = (uint)macAddr.Length;
                if (SendARP((int)hostEntry.AddressList[0].Address, 0, macAddr,
               ref macAddrLen) != 0)
                    return null;
                StringBuilder macAddressString = new StringBuilder();
                for (int i = 0; i < macAddr.Length; i++)
                {
                    if (macAddressString.Length > 0)
                        macAddressString.Append(":");
                    macAddressString.AppendFormat("{0:x2}", macAddr[i]);
                }
                return macAddressString.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<NetworkAddressModel> GetClientInfo()
        {
            try
            {
                List<NetworkAddressModel> li = new List<NetworkAddressModel>();
                NetworkAddressModel network = new NetworkAddressModel();
                IPAddress ipAddress = IPAddress.Broadcast;
                PhysicalAddress actual = GetMacAddress(ipAddress);
                network.IpAddress = GetIPAddress();
                // utilsM.IPADDRESS = ipAddress.;
                network.MacAddress = actual.ToString();
                li.Add(network);
                return li;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class NetworkAddressModel
    {
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
    }
}