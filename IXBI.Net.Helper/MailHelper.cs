﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace IXBI.Net.Helper
{
    public class MailHelper
    {
        public string MailServer { get; set; } = string.Empty;
        public int MailPort { get; set; } = 25;
        public string MailFrom { get; set; } = string.Empty;
        public string MailFromDisplayName { get; set; } = string.Empty;
        public string MailUsername { get; set; } = string.Empty;
        public string MailPassword { get; set; } = string.Empty;

        public List<MailAddress> SendTo { get; set; } = new List<MailAddress>();
        public List<MailAddress> SendBcc { get; set; } = new List<MailAddress>();
        public List<MailAddress> SendCC { get; set; } = new List<MailAddress>();
        public List<Attachment> AttachFile { get; set; } = new List<Attachment>();
        public string Subject { get; set; } = string.Empty;
        public string Body { get; set; } = string.Empty;
        public bool IsBodyHtml { get; set; } = true;

        public async Task SendMail()
        {
            try
            {
                using (SmtpClient smtpServer = new SmtpClient())
                {
                    smtpServer.Host = MailServer;
                    smtpServer.Port = MailPort;
                    smtpServer.UseDefaultCredentials = false;
                    smtpServer.EnableSsl = false;
                    smtpServer.Credentials = new NetworkCredential(MailUsername, MailPassword);

                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(MailFrom, MailFromDisplayName);
                        mail.IsBodyHtml = IsBodyHtml;
                        mail.Subject = Subject;
                        mail.Body = Body;

                        foreach (MailAddress sendTo in SendTo)
                        {
                            mail.To.Add(sendTo);
                        }

                        foreach (MailAddress sendCC in SendCC)
                        {
                            mail.CC.Add(sendCC);
                        }

                        foreach (MailAddress sendBcc in SendBcc)
                        {
                            mail.Bcc.Add(sendBcc);
                        }

                        foreach (Attachment attachFile in AttachFile)
                        {
                            mail.Attachments.Add(attachFile);
                        }

                        await smtpServer.SendMailAsync(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
