﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IXBI.Net.Helper
{
    public class ConfigurationHelper
    {
        #region Configuration Method

        public static string GetAppSettings(string key)
        {
            try
            {
                string value = StringHelper.TrimString(ConfigurationManager.AppSettings[key]);
                return value;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
