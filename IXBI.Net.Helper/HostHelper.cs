﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IXBI.Net.Helper
{
    public class HostHelper
    {
        public static string ApplicationPath()
        {
            try
            {
                string appPath = string.Empty;
                HttpContext context = HttpContext.Current;

                if (context != null)
                {
                    string port = context.Request.Url.Port == 80 ? String.Empty : ":" + context.Request.Url.Port.ToString();
                    appPath = String.Format("{0}://{1}{2}{3}", context.Request.Url.Scheme, context.Request.Url.Host, port, context.Request.ApplicationPath);
                }

                if (appPath.EndsWith("/") == false)
                {
                    appPath += "/";
                }

                return appPath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Dictionary<string, object> GetPostParameter(HttpRequestBase request)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();

                for (int i = 0; i < request.Form.Keys.Count; i++)
                {
                   string key = request.Form.Keys[i];
                   object value = request.Form[key];

                    if (key.Contains("sort"))
                    {
                        parameters.Add("sort", value);

                        key = key.Replace("sort", string.Empty);
                        key = key.Replace("[", string.Empty);
                        key = key.Replace("]", string.Empty);

                        parameters.Add("sender", key);
                    }
                    else
                    {
                        parameters.Add(key, value);
                    }
                }

                return parameters;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static NetworkAddressModel GetInformationClient()
        //{
        //    try
        //    {
        //        NetworkHelper network = new NetworkHelper();
        //        string ipAddress = string.Empty;
        //        string macAddress = string.Empty;

        //        List<NetworkAddressModel> networkModelList = network.GetInforClient();
        //        NetworkAddressModel networkModel = new NetworkAddressModel();

        //        if (networkModelList.Count > 0)
        //        {
        //            networkModel = networkModelList[0];
        //        }

        //        return networkModel;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
