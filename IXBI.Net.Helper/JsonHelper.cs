﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IXBI.Net.Helper
{
    public class JsonHelper
    {
        public static string SerializeObject(object data)
        {
            try
            {
                string strJson = JsonConvert.SerializeObject(data);

                return strJson;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static T DeserializeObject<T>(string data)
        {
            try
            {
                T obj = JsonConvert.DeserializeObject<T>(data);

                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
