﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using iTextSharp.text.pdf;
using System.Drawing;
using System.Drawing.Imaging;
using QRCoder;

namespace IXBI.Net.Helper
{
    public class QRcodeHelper
    {
        public string Level { get; set; } = "L";
        public string IconPath { get; set; } = string.Empty;
        public int IconSize { get; set; } = 0;

        public Bitmap RenderQrCodeToImage(string text)
        {
            try
            {
                QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(Level == "L" ? 0 : Level == "M" ? 1 : Level == "Q" ? 2 : 3);
                using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
                {
                    using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, eccLevel))
                    {
                        using (QRCode qrCode = new QRCode(qrCodeData))
                        {

                            Bitmap bitmap = qrCode.GetGraphic(20, Color.Black, Color.White,
                                GetIconBitmap(), IconSize);

                            return bitmap;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public byte[] RenderQrCodeToByte(string text)
        {
            try
            {
                Bitmap image = RenderQrCodeToImage(text);
                byte[] qrcodeByte = null;
                if (image != null)
                {
                    using (MemoryStream mStream = new MemoryStream())
                    {
                        image.Save(mStream, ImageFormat.Bmp);
                        qrcodeByte = mStream.ToArray();
                    }
                }

                return qrcodeByte;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string RenderQrCodeToBase64String(string text)
        {
            try
            {
                byte[] imageBytes = RenderQrCodeToByte(text);
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Bitmap GetIconBitmap()
        {
            try
            {
                Bitmap img = null;
                if (IconPath.Length > 0)
                {
                    img = new Bitmap(IconPath);
                }

                return img;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
