﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IXBI.Net.Helper
{
    public class SendGridHelper
    {
        public string SendGridKey { get; set; } = string.Empty;
        public string EmailFromAddress { get; set; } = string.Empty;
        public string EmailFromName { get; set; } = string.Empty;
        public List<EmailAddress> EmailSendTo { get; set; } = new List<EmailAddress>();
        public List<EmailAddress> EmailSendCC { get; set; } = new List<EmailAddress>();
        public List<EmailAddress> EmailSendBcc { get; set; } = new List<EmailAddress>();
        public List<Attachment> AttachFile { get; set; } = new List<Attachment>();
        public string Subject { get; set; } = string.Empty;
        public string Body { get; set; } = string.Empty;
        public bool IsBodyHtml { get; set; } = true;

        public async Task<Response> SendEmailAsync()
        {
            try
            {
                var client = new SendGridClient(SendGridKey);
                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress(EmailFromAddress, EmailFromName));
                if (EmailSendTo.Count > 0)
                    msg.AddTos(EmailSendTo);
                if (EmailSendCC.Count > 0)
                    msg.AddCcs(EmailSendCC);
                if (EmailSendBcc.Count > 0)
                    msg.AddBccs(EmailSendBcc);
                if (AttachFile.Count > 0)
                    msg.AddAttachments(AttachFile);

                msg.SetSubject(Subject);

                if (IsBodyHtml)
                    msg.AddContent(MimeType.Html, Body);
                else
                    msg.AddContent(MimeType.Text, Body);

                var response = await client.SendEmailAsync(msg);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
