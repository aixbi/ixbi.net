﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IXBI.Net.Helper
{
    public class ExcelHelper
    {
        #region Read excel file

        public static DataTable ReadToDataTable(string path)
        {
            try
            {
                FileInfo excel = new FileInfo(path);
                using (var package = new ExcelPackage(excel))
                {
                    var workbook = package.Workbook;

                    //*** Sheet 1
                    var worksheet = workbook.Worksheets.First();

                    //*** DataTable & DataSource
                    DataTable dt = ConvertToDataTable(worksheet);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable ReadToDataTable(string path, int sheet)
        {
            try
            {
                FileInfo excel = new FileInfo(path);
                using (var package = new ExcelPackage(excel))
                {
                    var workbook = package.Workbook;

                    //*** Sheet 1
                    var worksheet = workbook.Worksheets[sheet];

                    //*** DataTable & DataSource
                    DataTable dt = ConvertToDataTable(worksheet);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<T> ReadToObjectList<T>(string path)
        {
            try
            {
                FileInfo excel = new FileInfo(path);
                using (var package = new ExcelPackage(excel))
                {
                    var workbook = package.Workbook;

                    //*** Sheet 1
                    var worksheet = workbook.Worksheets.First();

                    //*** DataTable & DataSource
                    DataTable dt = ConvertToDataTable(worksheet);

                    List<T> objList = ModelHelper.CreateObjectList<T>(dt);

                    return objList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<T> ReadToObjectList<T>(string path, int sheet)
        {
            try
            {
                FileInfo excel = new FileInfo(path);
                using (var package = new ExcelPackage(excel))
                {
                    var workbook = package.Workbook;

                    //*** Sheet 1
                    var worksheet = workbook.Worksheets[sheet];

                    //*** DataTable & DataSource
                    DataTable dt = ConvertToDataTable(worksheet);

                    List<T> objList = ModelHelper.CreateObjectList<T>(dt);

                    return objList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ExcelWorksheets GetWorksheet(string path)
        {
            try
            {
                FileInfo excel = new FileInfo(path);
                using (var package = new ExcelPackage(excel))
                {
                    var workbook = package.Workbook;
                    var worksheets = workbook.Worksheets;

                    return worksheets;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable ReadWorksheetToDataTable(ExcelWorksheet oSheet)
        {
            try
            {
                DataTable dt = ConvertToDataTable(oSheet);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<T> ReadWorksheetToObjectList<T>(ExcelWorksheet oSheet)
        {
            try
            {
                DataTable dt = ConvertToDataTable(oSheet);
                List<T> objList = ModelHelper.CreateObjectList<T>(dt);

                return objList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static DataTable ConvertToDataTable(ExcelWorksheet oSheet)
        {
            try
            {
                int totalRows = oSheet.Dimension.End.Row;
                int totalCols = oSheet.Dimension.End.Column;
                DataTable dt = new DataTable(oSheet.Name);
                DataRow dr = null;

                for (int i = 1; i <= totalRows; i++)
                {
                    if (i > 1) dr = dt.Rows.Add();
                    for (int j = 1; j <= totalCols; j++)
                    {
                        if (i == 1)
                            dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                        else
                            dr[j - 1] = oSheet.Cells[i, j].Value.ToString();
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Write excel file



        #endregion
    }
}
