﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IXBI.Net.Helper
{
    public class XmlHelper
    {
        public static void WriteObjectXmlFile<T>(T model, string path)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(path))
                {
                    new XmlSerializer(typeof(T)).Serialize(sw, model);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static T ReadXmlFileObject<T>(string path)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    object objXml = new XmlSerializer(typeof(T)).Deserialize(sr);

                    return (T)objXml;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static T ReadXmlTextObject<T>(string data)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data)))
                {
                    object objXml = new XmlSerializer(typeof(T)).Deserialize(ms);

                    return (T)objXml;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
