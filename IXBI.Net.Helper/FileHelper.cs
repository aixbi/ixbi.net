﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IXBI.Net.Helper
{
    public class FileHelper
    {
        public static void CreateDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path) == false)
                    Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void DeleteDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                    Directory.Delete(path, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void WriteTextFile(string path, string text, bool IsAppend)
        {
            try
            {
                if (File.Exists(path))
                {
                    if (IsAppend)
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.Write(text);
                        }
                    }
                    else
                    {
                        DeleteFile(path);
                        using (StreamWriter sw = File.CreateText(path))
                        {
                            sw.Write(text);
                        }
                    }
                }
                else
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.Write(text);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void WriteLineTextFile(string path, string text, bool IsAppend)
        {
            try
            {
                if (File.Exists(path))
                {
                    if (IsAppend)
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.WriteLine(text);
                        }
                    }
                    else
                    {
                        DeleteFile(path);
                        using (StreamWriter sw = File.CreateText(path))
                        {
                            sw.WriteLine(text);
                        }
                    }
                }
                else
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(text);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static byte[] ReadBinaryFile(string path)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader stream = new BinaryReader(fs))
                    {
                        long lengthOfBuffer = fs.Length;
                        byte[] buff = new byte[lengthOfBuffer];

                        for (long i = 0; i < lengthOfBuffer; i++)
                        {
                            buff[i] = stream.ReadByte();
                        }

                        return buff;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static byte[] ReadBinaryUrlFile(string url)
        {
            try
            {
                using (var client = new WebClient())
                {
                    var file = client.DownloadData(url);
                    return file;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string ReadTextFile(string path, string encodingFormat)
        {
            try
            {
                string data = string.Empty;
                Encoding encoding = Encoding.Default;

                switch (encodingFormat)
                {
                    case "UTF8":
                        {
                            encoding = Encoding.UTF8;
                        }
                        break;
                    case "ASCII":
                        {
                            encoding = Encoding.ASCII;
                        }
                        break;
                    case "874":
                        {
                            encoding = Encoding.GetEncoding(874);
                        }
                        break;
                    default:
                        {
                            encoding = Encoding.Default;
                        }
                        break;
                }

                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader streamReader = new StreamReader(fs, encoding))
                    {
                        data = streamReader.ReadToEnd();
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<string> ReadLineTextFile(string path, string encodingFormat)
        {
            try
            {
                List<string> lines = new List<string>();
                Encoding encoding = Encoding.Default;

                switch (encodingFormat)
                {
                    case "UTF8":
                        {
                            encoding = Encoding.UTF8;
                        }
                        break;
                    case "ASCII":
                        {
                            encoding = Encoding.ASCII;
                        }
                        break;
                    case "874":
                        {
                            encoding = Encoding.GetEncoding(874);
                        }
                        break;
                    default:
                        {
                            encoding = Encoding.Default;
                        }
                        break;
                }

                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader streamReader = new StreamReader(fs, encoding))
                    {
                        string line = String.Empty;
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            lines.Add(line);
                        }
                    }
                }

                return lines;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string ReadBinaryFileToBase64String(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    byte[] file = ReadBinaryFile(path);
                    string base64String = Convert.ToBase64String(file);
                    return base64String;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string ReadBinaryFileUrlToBase64String(string url)
        {
            try
            {
                byte[] file = ReadBinaryUrlFile(url);
                string base64String = Convert.ToBase64String(file);
                return base64String;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
