﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IXBI.Net.Helper
{
    public class UploadHelper
    {
        public static List<FileUpload> LocalStorage(HttpFileCollection httpFiles, string destinationDirectory)
        {
            try
            {
                if (httpFiles == null || httpFiles.Count == 0)
                {
                    throw new Exception("File upload not found.");
                }

                FileHelper.CreateDirectory(destinationDirectory);

                List<FileUpload> fileUploadList = new List<FileUpload>();

                for (int i = 0; i < httpFiles.Count; i++)
                {
                    var postFile = httpFiles[i];
                    FileInfo file = new FileInfo(postFile.FileName);
                    string fileName = string.Format("{0}{1}", Guid.NewGuid().ToString().ToUpper(), file.Extension);
                    string filePath = string.Format("{0}{1}", destinationDirectory, fileName);

                    FileUpload fileUpload = new FileUpload();
                    fileUpload.OldFileName = file.Name;
                    fileUpload.FileName = fileName;
                    fileUpload.FileSize = postFile.ContentLength;

                    fileUploadList.Add(fileUpload);

                    postFile.SaveAs(filePath);
                }

                return fileUploadList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<FileUpload> LocalStorage(HttpFileCollectionBase httpFiles, string destinationDirectory)
        {
            try
            {
                if (httpFiles == null || httpFiles.Count == 0)
                {
                    throw new Exception("File upload not found.");
                }

                FileHelper.CreateDirectory(destinationDirectory);

                List<FileUpload> fileUploadList = new List<FileUpload>();

                for (int i = 0; i < httpFiles.Count; i++)
                {
                    var postFile = httpFiles[i];
                    FileInfo file = new FileInfo(postFile.FileName);
                    string fileName = string.Format("{0}{1}", Guid.NewGuid().ToString().ToUpper(), file.Extension);
                    string filePath = string.Format("{0}{1}", destinationDirectory, fileName);

                    FileUpload fileUpload = new FileUpload();
                    fileUpload.OldFileName = file.Name;
                    fileUpload.FileName = fileName;
                    fileUpload.FileSize = postFile.ContentLength;

                    fileUploadList.Add(fileUpload);

                    postFile.SaveAs(filePath);
                }

                return fileUploadList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static CloudBlobContainer GetCloudBlobContainer(string azureStorageConnectionString, string azureStorageContainer)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(azureStorageConnectionString);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(azureStorageContainer);
                return container;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<FileUpload> UploadStorageBlob(HttpFileCollection httpFiles, string azureStorageConnectionString, string azureStorageContainer)
        {
            try
            {
                List<FileUpload> fileUploadList = new List<FileUpload>();

                if (httpFiles != null)
                {
                    CloudBlobContainer container = GetCloudBlobContainer(azureStorageConnectionString, azureStorageContainer);

                    for (int i = 0; i < httpFiles.Count; i++)
                    {
                        var postFile = httpFiles[i];
                        string fileName = string.Format("{0}{1}", Guid.NewGuid().ToString().ToUpper(), Path.GetExtension(postFile.FileName));

                        CloudBlockBlob blob = container.GetBlockBlobReference(fileName);
                        using (var fileStream = postFile.InputStream)
                        {
                            blob.UploadFromStream(fileStream);
                            FileUpload fileUpload = new FileUpload();
                            fileUpload.OldFileName = postFile.FileName;
                            fileUpload.FileName = fileName;
                            fileUpload.FileSize = postFile.ContentLength;
                            fileUploadList.Add(fileUpload);
                        }
                    }
                }

                return fileUploadList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<FileUpload> UploadStorageBlob(HttpFileCollectionBase httpFiles, string azureStorageConnectionString, string azureStorageContainer)
        {
            try
            {
                List<FileUpload> fileUploadList = new List<FileUpload>();

                if (httpFiles != null)
                {
                    CloudBlobContainer container = GetCloudBlobContainer(azureStorageConnectionString, azureStorageContainer);

                    for (int i = 0; i < httpFiles.Count; i++)
                    {
                        var postFile = httpFiles[i];
                        string fileName = string.Format("{0}{1}", Guid.NewGuid().ToString().ToUpper(), Path.GetExtension(postFile.FileName));

                        CloudBlockBlob blob = container.GetBlockBlobReference(fileName);
                        using (var fileStream = postFile.InputStream)
                        {
                            blob.UploadFromStream(fileStream);
                            FileUpload fileUpload = new FileUpload();
                            fileUpload.OldFileName = postFile.FileName;
                            fileUpload.FileName = fileName;
                            fileUpload.FileSize = postFile.ContentLength;
                            fileUploadList.Add(fileUpload);
                        }
                    }
                }

                return fileUploadList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void DeleteStorageBlob(string fileName, string azureStorageConnectionString, string azureStorageContainer)
        {
            try
            {
                CloudBlobContainer container = GetCloudBlobContainer(azureStorageConnectionString, azureStorageContainer);
                CloudBlockBlob blob = container.GetBlockBlobReference(fileName);
                blob.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetStorageBlobUrl(string fileName, string azureStorageConnectionString, string azureStorageContainer)
        {
            try
            {
                CloudBlobContainer container = GetCloudBlobContainer(azureStorageConnectionString, azureStorageContainer);
                CloudBlockBlob blob = container.GetBlockBlobReference(fileName);
                string url = blob.Uri.AbsoluteUri;

                return url;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class FileUpload
    {
        public string FileName { get; set; } = string.Empty;
        public string OldFileName { get; set; } = string.Empty;
        public int FileSize { get; set; } = 0;
    }
}
